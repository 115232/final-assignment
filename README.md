Please use pip install - r requirements. txt to install the requirements. txt file before running the code\

python version: 3.9.17\


zip_path：Directory location containing 60 zip files\
base_path: Project Root Directory\
csv_base_path = Target location for CSV decompression\

Project Root(base_path)\
------- zip_path\
    |\
    --------------xx1.zip\
    --------------xx2.zip\
------- csv_base_path\
    |\
    --------------xx1.csv\
    --------------xx2.csv\
------- onpremises.ipynb\
------- readme.md\

